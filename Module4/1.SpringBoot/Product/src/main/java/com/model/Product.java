package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*; 

@Entity

public class Product {
	//acting as a primary key column
	@Id
	@GeneratedValue			//(strategy=GenerationType.AUTO)
	private int prodId;
	
	@Column(name="prodName")
	private String prodName;
	private String price;
	
	public Product() {
	}

	public Product(int prodId, String prodName, String price) {
		this.prodId = prodId;
		this.prodName = prodName;
		this.price = price;
	}

	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", prodName=" + prodName + ", price=" + price + "]";
	}
}