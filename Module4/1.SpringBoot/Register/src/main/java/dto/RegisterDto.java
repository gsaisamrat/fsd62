package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterDto {
	
	private int id;
	private String name;
	private String email;
}
