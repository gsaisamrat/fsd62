package com.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.CourseRepository;
import com.model.Course;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping("/")
    public Iterable<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @PostMapping("/")
    public Course createCourse(@RequestBody Course course) {
        return courseRepository.save(course);
    }

    // Add more endpoints for updating, deleting, etc. as needed
}
