package com.dao;

import com.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Component
@Transactional
public class CourseDao {

    @Autowired
    private EntityManager entityManager;

    public void saveCourse(Course course) {
        entityManager.persist(course);
    }

   
}
