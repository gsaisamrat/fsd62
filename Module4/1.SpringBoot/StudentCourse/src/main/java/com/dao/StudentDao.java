package com.dao;

import com.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Component
@Transactional
public class StudentDao {

    @Autowired
    private EntityManager entityManager;

    public void saveStudent(Student student) {
        entityManager.persist(student);
    }

    
}
