package com.model;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.mindrot.jbcrypt.BCrypt;

@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    
    
    @Column(unique = true)
	private String emailId;
	private String password;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    
    
    public Student() {
        
    }

    public Student(String name, String emailId, String password, Course course) {
        this.name = name;
        this.emailId = emailId;
        this.password = password;
        this.course = course;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
	    this.password = Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
	    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
	}
	
	public boolean checkPassword(String passwordToCheck) {
	    return BCrypt.checkpw(passwordToCheck, this.password);
	}

	
	

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", emailId=" + emailId + ", password=" + password + ", course="
				+ course + "]";
	}
    
}
