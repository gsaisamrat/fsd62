import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';

//For Two Way Data Binding
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { LogoutComponent } from './logout/logout.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    ShowemployeesComponent,
    ShowempbyidComponent,
    ExpPipe,
    GenderPipe,
    LogoutComponent,
    ProductComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,         //For Two Way Data Binding [(ngModel)]="person.id" under html page
    HttpClientModule,     // For Http Service(API Calls)
    RouterModule          //For Routing using the TypeScript
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
