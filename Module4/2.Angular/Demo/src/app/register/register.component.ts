import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
[x: string]: any;

  employee: any;
  countries: any;
  departments:any;

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
    this.employee = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',

      department: {
        deptId: ''
      }
    }
  }

  ngOnInit() {
    this.service.getAllContries().subscribe((data: any) => {this.countries = data;});
    this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
  }


  submit() {
    console.log(this.employee);
    this.service.registerEmployee(this.employee).subscribe((data: any) => {
      console.log(data);
    });
  }


}
