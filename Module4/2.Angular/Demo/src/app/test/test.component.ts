import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  age: number;

  hobbies: any;
  address: any;

  constructor() {
    this.id = 404;
    this.name = 'Sravan ';
    this.age = 21;

    this.hobbies = ['Running', 'Movies', 'Swimming','Dancing'];
    this.address = {streetNo: 213, city: 'Kandukur', state:'Andhra Pradesh'};
  }

  ngOnInit() {
  }

}
