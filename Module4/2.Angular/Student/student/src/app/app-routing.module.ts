import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowstudentsComponent } from './showstudents/showstudents.component';
import { ShowstudentbyidComponent } from './showstudentbyid/showstudentbyid.component';
import { LogoutComponent } from './logout/logout.component';


//Configure the routes variable using the routerLink and the respective component

const routes: Routes = [
  {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'showstus',    component:ShowstudentsComponent},
  {path:'showstubyid', component:ShowstudentbyidComponent},
  {path:'logout',      component:LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

