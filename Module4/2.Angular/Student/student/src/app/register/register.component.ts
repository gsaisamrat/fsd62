import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {

  student: any;

  constructor() {
    this.student = {
      stuId: '',
      stuName: '',
      fee: '',
      gender: '',
      dob: '',
      country: '',
      emailId: '',
      password: ''
    }
  }

  submit() {
    console.log(this.student);
  }

}



