import { Component } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showstudents.component.html',
  styleUrl: './showstudents.component.css'
})
export class ShowstudentsComponent {

  students: any; 

  //Date Format: mm-DD-YYYY
  constructor() {
    this.students = [
      {stuId:101, stuName:'Harsha', fees:10000, gender:'Male',   country:'IND', dob:'06-13-1960'},
      {stuId:102, stuName:'Pasha',  fees:20000, gender:'Male',   country:'CHI', dob:'07-14-1940'},
      {stuId:103, stuName:'Indira', fees:30000, gender:'Female', country:'USA', dob:'08-15-1930'},
      {stuId:104, stuName:'Navya',  fees:40000, gender:'Female', country:'FRA', dob:'09-16-1900'},
      {stuId:105, stuName:'Vamsi',  fees:50000, gender:'Male',   country:'NEP', dob:'10-17-1980'}
    ];
  }

}