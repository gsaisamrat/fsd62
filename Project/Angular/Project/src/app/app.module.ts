import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { GenderchangePipe } from './genderchange.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    GenderchangePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,


    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
