import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'genderchange'
})
export class GenderchangePipe implements PipeTransform {

  studName:any;
studGender:any;
  transform(name:any , gender:any): any {
    this.studName=name;
    this.studGender=gender;
    if(this.studGender=='male'){
      return "Mr. "+this.studName;
    }
    return "Miss. "+this.studName;
    
  }

}

