// googlesigninservice.service.ts
import { Injectable } from '@angular/core';

declare global {
  interface Window {
    gapi: any; // Add this line
  }
}

@Injectable({
  providedIn: 'root',
})
export class GoogleSignInService {
  private auth: any; // Use 'any' for the type

  constructor() {
    this.initGoogleSignIn();
  }

  
  
  private async initGoogleSignIn(): Promise<void> {
    console.log('Initializing Google Sign-In');
    try {
      await new Promise<void>((resolve) => {
        window.gapi.load('auth2', () => {
          window.gapi.auth2.init({
            client_id: '318863931977-feib8qf170fm1s8c7vntlpf3emjsgrnb.apps.googleusercontent.com',
            scope: "email",
          plugin_name:'tourismapp'
          }).then(() => {
            this.auth = window.gapi.auth2.getAuthInstance();
            resolve();
          }).catch((error: any) => {
            console.error('Error initializing Google Sign-In:', error);
            resolve(); // Resolve the promise even if there's an error to avoid unhandled promise rejection
          });
        });
      });
    } catch (error) {
      console.error('Unexpected error during Google Sign-In initialization:', error);
    }
  }
  
  

  async signInWithGoogle(): Promise<void> {
    if (!this.auth) {
      // Handle the case where auth is not initialized
      return;
    }
  
    const googleUser = await this.auth.signIn();
    const profile = googleUser.getBasicProfile();
    const user = {
      name: profile.getName(),
      email: profile.getEmail(),
      // Add other relevant information if needed
    };
  
    // Store user data in local storage
    localStorage.setItem("user", JSON.stringify(user));
  
    // Perform additional actions as needed
    console.log('User signed in:', user.name);
    // ...
  }
  
}

