import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
 
  registerUser(user: any): any {
    return this.http.post('registerUser', user);
  }

  loginStatus: Subject<any>;
  isUserLogged: boolean;

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) { 
    this.isUserLogged = false;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus = new Subject();
  }

  getCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

userLogin(loginForm: any) {
  return this.http.get("userLogin/" + loginForm.emailId + "/" + loginForm.password).toPromise();
}

 //Login
 setUserLogIn() {
  this.isUserLogged = true;

 
}
//Logout
setUserLogOut() {
  this.isUserLogged = false;

}

getLoginStatus(): boolean {
  return this.isUserLogged;
}




updateUserData(id: string, updatedUser: any): any {
  return this.http.put(`updateUser/${id}`, updatedUser);
}



deleteUserAccount(id: any): any {
  return this.http.delete('deleteUserById/' + id);
}

sendOtp(phonenumber: any): any {
  return this.http.get('send-sms/' + phonenumber);
}

verifyOtp(phonenumber: any, otp: any): any {
  return this.http.get('verify-otp/' + phonenumber + '/' + otp);
}


}

