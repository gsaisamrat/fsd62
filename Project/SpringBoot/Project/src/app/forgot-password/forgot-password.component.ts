import { Component } from '@angular/core';
import {NgToastService} from 'ng-angular-popup'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrl: './forgot-password.component.css'
})
export class ForgotPasswordComponent {

  constructor(private toast : NgToastService) {

  }



  loginSubmit(loginForm: any) {
    console.log(loginForm);
    this.toast.success({detail:"Success Message",summary:"Login Success",duration:5000})
    
  }
}
