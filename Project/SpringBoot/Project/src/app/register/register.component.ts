import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import {NgToastService} from 'ng-angular-popup'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'

})


export class RegisterComponent {

  form: FormGroup;

  constructor(private toast : NgToastService) {
    this.form = new FormGroup (
      {
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required])
    }, 

    {
      validators: this.passwordMatchValidator,

    }

  );
  }

  passwordMatchValidator(control : AbstractControl) {
       return control.get('password')?.value === control.get('confirmPassword')?.value ? null : {mismatch : true};
       this.toast.success({detail:"Success Message",summary:"Login Success",duration:5000})
      }

 }


















// import { Component } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { AuthService } from '../auth.service';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent {
//   registerForm: FormGroup;


//   constructor(private fb: FormBuilder, private authService: AuthService) {
//     this.registerForm = this.fb.group({
//       email: ['', [Validators.required, Validators.email]],
//       password: ['', Validators.required],
//       confirmPassword: ['', Validators.required]
//     });
//   }

//   onSubmit() {
//     if (this.registerForm.valid) {
//       const email = this.registerForm.value.email;
//       const password = this.registerForm.value.password;
//       const confirmPassword = this.registerForm.value.confirmPassword;

//       // Send registration data to backend for validation and registration
//       this.authService.register(email, password, confirmPassword).subscribe(
//         () => {
//           console.log('Registration successful');
//           // Redirect to login page or show success message
//         },
//         error  => {
//           console.error('Registration failed:', error);
//           // Handle registration failure (show error message, etc.)
//         }
//       );
//     }
//   }
// }


























